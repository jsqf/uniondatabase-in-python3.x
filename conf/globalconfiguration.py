#!/usr/bin/python2.7
#coding=utf8

class Config(object):
    "mysql oracle sqlserver sqlalchemy_oracle"
    DATABASENAME="oracle"   #数据库名字
    CSVFILEPATH="D:/uniondatabase/csvfile/TableConstruction.csv" #csv 文件路径
    CSVFILECODING="gb2312" #csv 文件 编码
    CONN="oracle://yyb:123@localhost:1521/orcl"  # 数据库连接路径 
    ORACLESQLFILEPATH="D:/uniondatabase/sqlfile/oracle_sql.sql" #生成SQL 文件路径
    SQLALCHEMY_ORACLESQLFILEPATH="D:/uniondatabase/sqlfile/sqlalchemy_oracle_sql.py" #生成SQLALCHEMY_ORACLE.py 文件路径