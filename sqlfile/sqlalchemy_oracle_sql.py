#!/usr/bin/python2.7
#coding=utf8
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Index, Sequence, BigInteger, Integer, DateTime, Numeric, String, Unicode, UnicodeText, Text

Base = declarative_base()


class T_Ygb_Cjl (Base):
    __tablename__="T_YGB_CJL" 
    id = Column(BigInteger, Sequence("seq_t_ygb_cjl_id"),primary_key=True)
    yyb_id = Column(Integer, nullable=False)
    jzrq = Column(DateTime, nullable=False)
    tjzq = Column(Integer, nullable=False)
    ywzl = Column(Integer, nullable=False)
    cjl_zl = Column(Numeric(18,2), nullable=True)
    cjl_tb = Column(Numeric(10,4), nullable=True)
    cjl_hb = Column(Numeric(10,4), nullable=True)
    create_time = Column(DateTime, nullable=False ,default=datetime.datetime.now())
    update_time = Column(DateTime, nullable=False ,default=datetime.datetime.now())
Index('data_t_ygb_cjl_unique', T_Ygb_Cjl.yyb_id,T_Ygb_Cjl.jzrq,T_Ygb_Cjl.tjzq,T_Ygb_Cjl.ywzl, unique=True)



class T_Ygb_Scfe (Base):
    __tablename__="T_YGB_SCFE" 
    id = Column(BigInteger, Sequence("seq_t_ygb_scfe_id"),primary_key=True)
    yyb_id = Column(Integer, nullable=False)
    jzrq = Column(DateTime, nullable=False)
    tjzq = Column(Integer, nullable=False)
    ywzl = Column(Integer, nullable=False)
    fe_zl = Column(Numeric(18,2), nullable=True)
    fe_tb = Column(Numeric(10,4), nullable=True)
    fe_hb = Column(Numeric(10,4), nullable=True)
    create_time = Column(DateTime, nullable=False ,default=datetime.datetime.now())
    update_time = Column(DateTime, nullable=False ,default=datetime.datetime.now())
Index('data_t_ygb_scfe_unique', T_Ygb_Scfe.yyb_id,T_Ygb_Scfe.jzrq,T_Ygb_Scfe.tjzq,T_Ygb_Scfe.ywzl, unique=True)



