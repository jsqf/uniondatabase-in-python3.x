create table t_ygb_cjl (
id number(19) not null,
yyb_id integer not null,
jzrq date not null,
tjzq integer not null,
ywzl integer not null,
cjl_zl number(18,2),
cjl_tb number(10,4),
cjl_hb number(10,4),
create_time date not null,
update_time date not null,
constraint pk_t_ygb_cjl primary key (id) ,
constraint t_ygb_cjl_unique unique(yyb_id,jzrq,tjzq,ywzl) 
);
create table t_ygb_scfe (
id number(19) not null,
yyb_id integer not null,
jzrq date not null,
tjzq integer not null,
ywzl integer not null,
fe_zl number(18,2),
fe_tb number(10,4),
fe_hb number(10,4),
create_time date not null,
update_time date not null,
constraint pk_t_ygb_scfe primary key (id) ,
constraint t_ygb_scfe_unique unique(yyb_id,jzrq,tjzq,ywzl) 
);
