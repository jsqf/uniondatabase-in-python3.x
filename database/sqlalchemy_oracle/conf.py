#!/usr/bin/python2.7
#coding=utf8
import sys
class Dataformat(object):
    cx_oracle_head="""#!/usr/bin/python2.7
#coding=utf8
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Index, Sequence, BigInteger, Integer, DateTime, Numeric, String, Unicode, UnicodeText, Text

Base = declarative_base()\n\n\n"""
    def transfer(self,str):
        formatindex={"int":"Integer","date":"DateTime","datetime":"DateTime","bigint":"BigInteger"}#string(n)->varchar2(n)  decimal(m,n)->Numeric(m,n)
        str=str.replace('"',"")
        if str in formatindex:
            return formatindex[str]
        elif str[0:str.find("(")] == "string":
            return "varchar2("+str[str.find("(")+1:str.find(")")]+")"
        elif str[0:str.find("(")] == "decimal":                            
            xx="Numeric("+str[str.find("(")+1:str.find(")")]+")"
            #index=xx.find(".")
            #xx=unicode(xx,"utf8")
            xx=xx.replace(".",",")
            return xx
        else :
            print(str[0:str.find("(")])
            print("ERROR! no this data format  at  database/oracle/conf")
            sys.exit(1)