#!/usr/bin/python2.7
#coding=utf8
import sys
class Dataformat(object):
    
    def transfer(self,str):
        formatindex={"int".encode('utf8'):"integer".encode('utf8'),"date".encode('utf8'):"date".encode('utf8'),"datetime".encode('utf8'):"date".encode('utf8'),"bigint".encode('utf8'):"number(19)".encode('utf8')}#string(n)->varchar2(n)  decimal(m,n)->number(m,n)
        str=str.replace('"'.encode('utf8'),"".encode('utf8'))
        if str in formatindex:
            return formatindex[str]
        elif str[0:str.find("(".encode('utf8'))] == "string".encode('utf8'):
            return "varchar2(".encode('utf8')+str[str.find("(".encode('utf8'))+1:str.find(")".encode('utf8'))]+")".encode('utf8')
        elif str[0:str.find("(".encode('utf8'))] == "decimal".encode('utf8'):
            xx="number(".encode('utf8')+str[str.find("(".encode('utf8'))+1:str.find(")".encode('utf8'))]+")".encode('utf8')
            #index=xx.find(".")
            #xx=unicode(xx,"utf8")
            xx=xx.replace(".".encode('utf8'),",".encode('utf8'))
            return xx
        else :
            print(str[0:str.find("(".encode('utf8'))])
            print("ERROR! no this data format  at  database/oracle/conf")
            sys.exit(1)